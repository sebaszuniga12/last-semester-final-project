#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define TAM 10	 //Dimension de vector para longitud de c�dula

using namespace std;

struct Registro
{
	long int cedula;
	string nombre;
	string apellido;
	int edad;
};

struct Condena
{
	int codigo;
	double diasrestantes;
	string cargos;
};

struct Reclusos
{
	Registro reg;
	Condena con;
};

int count=0;

Reclusos pr[50];
Reclusos sob;

void establecido();
void nuevo_caso(Reclusos op[]);
void imprbl();
void menu2();
double dias_anios(double dias);
bool cedrev(long int cedula);
bool encabezado();
void modrec(Reclusos* op);
bool cedula(long int cedula);

void conteo()
{
	if(count!=0)
	{
		printf("\n\nHay %i presos registrados\n\n",count);
	}
	else
	{
		printf("\n\nNo hay reclusos en el registro\n\n");
	}
}
void imprbl()
{
	FILE* lectura;
	char buffer;
	int ced1=0;
	for(int i=0; i<49; i++)
	{
		if(pr[i].reg.cedula==1)
		{
			ced1++;
		}
		
	}
	if(ced1==49)
	{
		system("cls");
		printf("ERROR: No se puede imprimir un archivo sin haber agregado prisioneros\n");
		system("pause");
		return;
	}
	
	lectura=fopen("informacion.txt","r");
	
	if(lectura==NULL)
	{
		system("cls");
		printf("ERROR: El archivo de reporte no se ha creado aun o hubo un error de lectura\n");
		system("pause");
		return;
	}
	
	system("cls");
	
	while((buffer = fgetc(lectura)) != EOF)
	{
		printf("%c",buffer);
	}
        
    fclose(lectura);
    printf("\n\n");
    system("pause");
}
void init()
{
	for(int i=0; i<49; i++)
	{
		pr[i].con.cargos="";
		pr[i].con.codigo=0;
		pr[i].con.diasrestantes=0;
		pr[i].reg.cedula=1;
		pr[i].reg.nombre="";
		pr[i].reg.apellido="";
		pr[i].reg.edad=0;
	}
}
bool cedula_sch(long int ced)
{
	FILE* info;
	info=fopen("informacion.txt","r");

}
bool cedrev(long int cedula)
{
	for(int i=0; i<49; i++)
	{
		if(cedula==pr[i].reg.cedula)
		{
			return true;

		}
	}
	
	return false;
}
void eliminar()
{
	bool ex;
	int ceda;
	int assig=0;
	int ag;
	int op;

	if(count==0)
	{
		system("cls");
		printf("ERROR: No se puede eliminar, por que no hay prisioneros disponibles\n");
		system("pause");
		return;
	}
	
	system("cls");
	system("color 0c");
	printf("\t\t=====ELIMINAR RECLUSO=====\n");
	printf("Ingrese la cedula del prisionero para eliminarlo del sistema: ");
	cin>>ceda;
	
	for(int i=0; i<49; i++)
	{		
		if(pr[i].reg.cedula==ceda)
		{
			assig=i;
			ag=i;
			ag++;
			ex=true;
			break;
		}
   	}
   	
   	if(ex)
   	{
   		printf("\nSe ha encontrado la cedula %ld en el ID %i\n",ceda,assig);
		printf("Con la siguiente informacion:\n");
		cout<<"\n["<<ag<<"]\n";
		cout<<"\nPreso CRJVG ";
		cout<<pr[assig].con.codigo<<":\n";
		cout<<"Nombre: ";
		cout<<pr[assig].reg.nombre<<"\n";
    	cout<<"Apellido: ";
		cout<<pr[assig].reg.apellido<<"\n";
   		cout<<"Edad: ";
   		cout<<pr[assig].reg.edad<<"\n";
   		cout<<"Cedula: ";
    	cout<<pr[assig].reg.cedula<<"\n";
    	cout<<"Cargos: ";
		cout<<pr[assig].con.cargos<<"\n";
		cout<<"Tiempo de condena: ";
		cout<<dias_anios(pr[assig].con.diasrestantes);
		cout<<" anios ("<<pr[assig].con.diasrestantes<<" dias restantes)\n\n";
			
		printf("Desea eliminarlo?\n");
		printf("1.Si\n");
		printf("2.No\n\n");
		scanf("%i",&op);
			
		switch(op)
		{
			case 1:
				pr[assig].reg.cedula=0;
				count--;
				system("cls");
				printf("Prisionero eliminado exitosamente, actualize el reporte para ver los cambios\n\n");
				system("pause");
				return;
				break;
			default:
				system("cls");
				printf("Ningun cambio realizado\n");
				system("pause");
				return;
				break;	
			}
			
	}
	else
	{
		system("cls");
		printf("ERROR: No se ha encontrado a ningun recluso con dicha cedula propuesta\n\n");
		system("pause");
	}
}
void modifmenu()
{
	int disp=0;
	int ag=0;
	int assig=0;
	bool cedexs=false;
	int selec=0;
	long int ver;
	
	if(count==0)
	{
		system("cls");
		printf("ERROR: No se puede modificar, por que no hay prisioneros disponibles\n");
		system("pause");
		return;
	}
	
	system("cls");
	printf("\t\t===============REGISTRO CRIMINAL- MODIFICAR RECLUSO===============\n\n");
	
	printf("Ingrese una cedula existente en el sistema: ");
	cin>>ver;
	
	for(int i=0; i<49; i++)
	{
		ag=i;
		ag++;
		
		if(ver==pr[i].reg.cedula)
		{
			printf("\n\nSe ha encontrado la cedula %ld con la siguiente informacion",ver);	
			cout<<"\n["<<ag<<"]\n";
			cout<<"\nPreso CRJVG ";
			cout<<pr[i].con.codigo<<" :\n";
			cout<<"Nombre: ";
			cout<<pr[i].reg.nombre<<"\n";
   			cout<<"Apellido: ";
			cout<<pr[i].reg.apellido<<"\n";
   			cout<<"Edad: ";
    		cout<<pr[i].reg.edad<<"\n";
 			cout<<"Cedula: ";
  			cout<<pr[i].reg.cedula<<"\n";
    		cout<<"Cargos: ";
			cout<<pr[i].con.cargos<<"\n";
			cout<<"Tiempo de condena: ";
			cout<<dias_anios(pr[i].con.diasrestantes);
			cout<<" anios ("<<pr[i].con.diasrestantes<<" dias restantes)\n\n";
			
			printf("Que desea hacer?\n");
			printf("1. Sobrescribir todos los datos\n");
			printf("2. Enviar Prisionero actual a otro casillero y sobrescribir este\n");
			printf("3. Nada\n\n");
			scanf("%i",&selec);
			fflush(stdin);
			
			switch(selec)
			{
				case 1:
					modrec(&pr[i]);
					return;
					break;
				case 2:
					for(int j=0; j<49; j++)
					{		
						if((pr[j].reg.cedula==0)||(pr[j].reg.cedula==1)){}
   						else
   						{
   							disp++;
						}
   					}
   	
   					if(disp==49)
   					{
   						printf("ERROR: No hay espacio para reclusos, la carcel esta llena\n");
   						printf("Ningun cambio realizado\n\n");
   						system("pause");
   						return;
 					}
 					for(int k=0; k<49; k++)  
					{		
						if(pr[k].reg.cedula==0||pr[k].reg.cedula==1)
   						{
   							assig=k;
   							break;
   						}
   					}
   					
   					count++;
   					sob=pr[i];
   					pr[assig]=sob;
   					modrec(&pr[i]);
			  	
				default:
					printf("Ningun cambio realizado\n\n");
					return;
					break;	
			}
			 
		}
	
	}
	system("cls");
	printf("ERROR: Recluso no se ha encontrado con dicha cedula\n");	
	system("pause");
	
}
void modrec(Reclusos *op)
{
	int d,m,a,ver,control;
	int opc;
	int disp;
	int assig;
	int numeral;
	bool mismac=false;
	bool cedexs=false;
	bool val=false;
	int ID=0;
	int ag=0;
	printf("\n\n");
	
	printf("Desea re-generar el codigo de recluso?\n");
	printf("1. Si\n");
	printf("2. No\n\n");
	scanf("%i",&opc);
	fflush(stdin);
	
	switch(opc)
	{
		case 1:
			srand (time(0));
			numeral=rand() % 100 + 1 ;
			printf("El codigo del prisionero es CRJVG: %d\n",numeral);
			op->con.codigo=numeral;
			system("pause");
			break;
		default:
			printf("Ningun cambio concretado\n");
			system("pause");
			break;
	}

	do
	{
		do
		{
			system("cls");
			printf("\nIngrese la cedula del recluso para modificar: ");
			cin>>ver;
		
			if(op->reg.cedula==ver)
			{
				mismac=true;
				printf("ERROR: Dicha cedula es la misma para este recluso\n");
				system("pause");
			}
			else
			{
				mismac=false;
			}
			
		}while(mismac);
		
		if(cedrev(ver))
		{
			cedexs=true;
			system("cls");
			printf("ERROR: Dicha cedula ya existe en el sistema\n");
			system("pause");
		}
		else
		{
				
			if(cedula(ver))
			{
				op->reg.cedula=ver;
				system("cls");
				cedexs=false;
			}
			else
			{
				system("cls");
				printf("El numero de cedula esta errado o es falso\n");
				system("pause");
				system("cls");
				cedexs=true;

			}
		
		}
	
	}while(cedexs);
	
	
	printf("\nIngrese el nombre del recluso: ");
 	cin>>op->reg.nombre;

	printf("\nIngrese el apellido del recluso: ");
 	cin>>op->reg.apellido;

	do
	{
		printf("\nIngrese la edad: ");
		cin>>op->reg.edad;
		control=op->reg.edad;
		if((control<18)||(control>99))
		{
			printf("La edad no esta permitida o la edad no esta correcta");
			op->reg.edad=0;
			control=0;
			system("pause");
			system("cls");
		}

	}while(control<18);
	printf("\nIngrese el cargo del recluso: ");
	cin>>op->con.cargos;
	fflush(stdin);
	time_t currentTime;
	time(&currentTime);
	system("cls");
	printf("\nTiempo de inicio de condena: %s\n",ctime(&currentTime));
	printf("\n========FECHA DE SALIDA========\n");
	printf("\nIngrese la fecha de salida\n");

	do
	{
		printf("\nAnio: 20");
		scanf("%d",&a);
		printf("\n");
			
		if(a<=17 || a>99)
		{
			val=true;
			printf("\nExcede del a�o permitido o dicho a�o es anterior a la fecha de hoy\n");
			a=0;
			system("pause");
			system("cls");
		}
		else if(a==18)
		{
			val=false;
			do
			{
				printf("Mes: ");
				scanf("%d",&m);
				fflush(stdin);

				if(m<=7 || m>12)
				{
					val=true;
					printf("\nEl mes ingresado no es valido o dicho mes es anterior del mes actual\n");
					m=0;
					system("pause");
					system("cls");
				}
				else if(m==8)
				{
					val=false;
					do
					{
						printf("\nDia: ");
						scanf("%d",&d);
						fflush(stdin);

						if(d<=13 || d>31)
						{
							val=true;
							printf("\nEl dia ingresado no es valido o dicho dia es anterior al dia actual\n");
							d=0;
							system("pause");
							system("cls");
						}
						else
						{
							val=false;
						}

					}while(val);
				}
				else
				{
						printf("\nDia: ");
						scanf("%d",&d);
						fflush(stdin);

						if(d<=01 || d>31)
						{
							val=true;
							printf("\nEl dia ingresado no es valido\n");
							d=0;
							system("pause");
							system("cls");
						}
						else
						{
							val=false;
						}
				}
				
			}while(val);
							
		}
		else
		{
			do
			{
				printf("\nMes: ");
				scanf("%d",&m);

				if(m<1 || m>12)
				{
					val=true;
					printf("\nEl mes ingresado no es valido\n");
					m=0;
					system("pause");
					system("cls");
				}
				else
				{
					val=false;
				}
			}while(val);
			
			do
			{
				printf("\nDia: ");
				scanf("%d",&d);

				if(d<1 || d>31)
				{
					val=true;
					printf("\nEl dia ingresado no es valido o dicho dia es anterior al dia actual\n");
					d=0;
					system("pause");
					system("cls");
				}
				else
				{
					val=false;
				}

			}while(val);
			
		}
	}while(val);

	
	if(a>=18)
	{
		double dias;
		time_t now;
		struct tm nuevo_a;
		double segundos;
		time(&now);
		nuevo_a = *localtime(&now);
		nuevo_a.tm_hour = 0; nuevo_a.tm_min = 0; nuevo_a.tm_sec = 0;
		nuevo_a.tm_year = (a+100);	nuevo_a.tm_mon = (m-1);  nuevo_a.tm_mday = (d+1);
		segundos = difftime(now,mktime(&nuevo_a));
		op->con.diasrestantes= (0-segundos)/86400;
		printf ("\n\n\nQuedan %.f dias restantes de condena a partir de hoy.\n", op->con.diasrestantes);
		system("pause");	
	}
		system("cls");
		printf("Datos del recluso editados correctamente\n");	
		system("pause");
}
void currbase(Reclusos op[])
{
	fstream db;
	int ced=0;
	int ag=0;
	int ver=0;
	
	for(int i=0; i<49; i++)
	{
		if(pr[i].reg.cedula==1)
		{
			ced++;
		}
		
	}
	if(ced==49)
	{
		system("cls");
		printf("ERROR: No se puede generar un archivo de reporte sin haber hecho transacciones antes\n");
		system("pause");
		return;

	}
	db.open("informacion.txt",ios::in);
	
	if(!db.fail())  //SI EL ARCHIVO EXISTE, APP
	{
		db.close();
		if(encabezado())
        {
        	db.open("informacion.txt",fstream::app);
        	for(int i=0; i<49; i++)
        	{
        		ag=i;
				ag++;
				
        		if(op[i].reg.cedula==0)
        		{
        			db<<"ID ["<<ag<<"] eliminado posteriormente\n" ;
				}
				
				else if (op[i].reg.cedula==1)
				{
					db<<"Vacio["<<ag<<"] \n";
				}
				else
				{
					db<<"["<<ag<<"]\n";
					db<<"Preso CRJVG ";
					db<<op[i].con.codigo<<":\n";
					db<<"Nombre: ";
					db<<op[i].reg.nombre<<"\n";
    				db<<"Apellido: ";
					db<<op[i].reg.apellido<<"\n";
    				db<<"Edad: ";
    				db<<op[i].reg.edad<<"\n";
    				db<<"Cedula: ";
    				db<<op[i].reg.cedula<<"\n";
    				db<<"Cargos: ";
					db<<op[i].con.cargos<<"\n";
					db<<"Tiempo de condena: ";
					db<<dias_anios(op[i].con.diasrestantes);
					db<<" anios ("<<op[i].con.diasrestantes<<" dias restantes)\n\n";
				}
				
			}
		db<<"\n\n -----FIN DE REGISTRO-----\n\n";	
		db.close();
        
		}
		else
		{
			db.close();	
		}
		
	}
	else //SI EL NO ARCHIVO EXISTE, SE CREA POR PRIMERA VEZ
	{
		if(encabezado())
        {
        	db.open("informacion.txt",fstream::out);
        	for(int i=0; i<49; i++)
        	{
        		ag=i;
				ag++;
				
        		if(op[i].reg.cedula==0)
        		{
        			db<<"ID ["<<ag<<"] eliminado posteriormente\n" ;
				}
				
				else if (op[i].reg.cedula==1)
				{
					db<<"Vacio["<<ag<<"] \n" ;
				}
				else
				{
					db<<"["<<ag<<"]\n";
					db<<"Preso CRJVG ";
					db<<op[i].con.codigo<<":\n";
					db<<"Nombre: ";
					db<<op[i].reg.nombre<<"\n";
    				db<<"Apellido: ";
					db<<op[i].reg.apellido<<"\n";
    				db<<"Edad: ";
    				db<<op[i].reg.edad<<"\n";
    				db<<"Cedula: ";
    				db<<op[i].reg.cedula<<"\n";
    				db<<"Cargos: ";
					db<<op[i].con.cargos<<"\n";
					db<<"Tiempo de condena: ";
					db<<dias_anios(op[i].con.diasrestantes);
					db<<" anios ("<<op[i].con.diasrestantes<<" dias restantes)\n\n";
				}
				
			}
		db<<"\n\n -----FIN DE REGISTRO-----\n\n";
		db.close();
        
		}
		else
		{
			db.close();	
		}
	}

}
double dias_anios(double dias)
{
	return (dias / 365);
}
void menu1()
{
	int opcion1;
	
	do
	{
	system("cls");
	system("color F1");
	printf("=====================================CENTRO RECLUSION VIRGILIO GUERRERO (Capacidad 50)=====================================\n");
	conteo();
	printf("1. Reportar y actualizar en archivo de texto los presos actuales\n");
	printf("2. Imprimir en pantalla la base de datos generalizada\n");
	printf("3. Agregar reclusos de manera predeterminada\n");
	printf("4. Agregar recluso al sistema\n");
	printf("5. Modificar datos de un recluso\n");
	printf("6. Eliminar recluso\n");
	printf("7. Salir del Sistema\n\n");
	
	printf("Por favor elija una opcion: ");
	scanf("%i",&opcion1);
	fflush(stdin);

		switch(opcion1)
	    {
	    	case 1:
				system("cls");
				currbase(pr);
				break;
			case 2:
				imprbl();
				system("cls");
				break;
			case 3:
				establecido();
				system("cls");
				break;
	    	case 4:
	    		menu2();
				system("cls");
				break;
			case 5:
				modifmenu();
				break;
			case 6:
				eliminar();
				break;
			case 7:
				return;
				break;
			default:
				printf("Opci�n no valida, ingrese de nuevo por favor\n");
				system("pause");
				system("cls");
				break;
		}
	}while(true);

}
void menu2()
{
	system("cls");
	system("color e1");
	int opcion2;
	do
	{

		printf("\t\t=====AGREGAR RECLUSO=====\n");
		printf("\n2.1 Nuevo caso");
		printf("\n2.2 Terminar sesion\n\n");
		scanf("%i",&opcion2);
		fflush(stdin);
		switch (opcion2)
		{
			case 1:
			{
				nuevo_caso(pr);
				break;
			}
			case 2:
			{
				return;
				break;
			}
			default:
			{
				return;
				break;
			}

		}
	}while(true);

}
bool encabezado()  //IMPRIMIR FECHA POR REGISTRO ACTUALIZADO
{
 //	auto t = std::time(nullptr);
   // auto tm = *std::localtime(&t);
    
	ofstream presos;
    presos.open("informacion.txt",ios::app);
    if (presos.fail())
	{
   	 	printf("ERROR: No se pudo abrir el archivo para generar el encabezado\n");
   	 	system("pause");
   	 	return false; 
   	}

    presos<<"                        CENTRO RECLUSION VIRGILIO GUERRERO             \n";
   // presos<<" Este reporte ha sido actualizado desde: " AQUI PONER FECHA Y HORA ACTUALIZADA A CADA QUE SE LLAME;
    
   	presos<<"\nA continuacion se encuentran la informacion de los reclusos que se encuentran en el centro de reclusion:";
    presos<<"\n                                      Reclusos                                  ";
    presos<<"\n";
   	presos.close();
   	return true;
}
void establecido()
{
	int acont=0;
	int disp=0;
	int assig=50;

	for(int i=0; i<49; i++)
	{		
		if((pr[i].reg.cedula==0)||(pr[i].reg.cedula==1)){}
   		else
   		{
   			disp++;
		}
   	}
   	
   	if(disp==49)
   	{
   		printf("ERROR: No hay espacio para reclusos, la carcel esta llena\n");
   		system("pause");
   		return;
 	}
 	
 		
	if(cedrev(1706855580))
	{
		system("cls");
	  	printf("ERROR: El primer preso predeterminado esta ya en el sistema\n");
	  	system("pause");
	}
	else
	{
		for(int i=0; i<49; i++)   //BUCLES ASIGNADORES PARA DISPONIBILIDAD
		{		
			if(pr[i].reg.cedula==0||pr[i].reg.cedula==1)
   			{
   				assig=i;
   				break;
   			}
   		}
   	
		if(!(assig==50))
    	{
    		system("cls");
			pr[assig].con.codigo=56;
			pr[assig].con.diasrestantes=3650;
			pr[assig].reg.cedula=1706855580;
			pr[assig].reg.nombre="Roberto";
			pr[assig].reg.apellido="Perez";
			pr[assig].reg.edad=25;
    		pr[assig].con.cargos="Asesino a 2 personas con una pistola ";
    		printf("Recluso con ID %i y codigo %i agregado de manera predeterminada\n\n",assig,pr[assig].con.codigo);
    		system("pause");
    		system("cls");
    		acont++;
	 		count++;
	 		assig=50;
		}
		else
		{
	 		system("cls");
			printf("ERROR: No hay espacio para reclusos, la carcel esta llena, no se agregar� m�s\n");
			system("pause");	
			return;	
		}
	}
 	
 	if(cedrev(1724589678))
	{
		system("cls");
	  	printf("ERROR: El segundo preso predeterminado esta ya en el sistema\n");
	  	system("pause");
	}
	else
	{
		for(int i=0; i<49; i++)   //BUCLES ASIGNADORES PARA DISPONIBILIDAD
		{		
			if(pr[i].reg.cedula==0||pr[i].reg.cedula==1)
   			{
   				assig=i;
   				break;
   			}
   		}
	
		if(!(assig==50))
   		{
			pr[assig].con.codigo=12;
			pr[assig].con.diasrestantes=2920;
			pr[assig].reg.cedula=1724589678;
			pr[assig].reg.nombre="Elvis";
			pr[assig].reg.apellido="Garces";
			pr[assig].reg.edad=40;
   	 		pr[assig].con.cargos="Asesino a 2 personas con una pistola ";
    		printf("Recluso con ID %i y codigo %i agregado de manera predeterminada\n\n",assig,pr[assig].con.codigo);
    		system("pause");
    		system("cls");
    		acont++;
		 	count++;
		 	assig=50;
		}
		
		else
		{
			system("cls");
			printf("ERROR: No hay espacio para reclusos, la carcel esta llena, pero se ha agregado %i reclusos exitosamente\n\n",acont);
			system("pause");
			return;		
		}	
	}
	
	if(cedrev(1602123341))
	{
		system("cls");
	  	printf("ERROR: El tercer preso predeterminado esta ya en el sistema\n");
	  	system("pause");
	}
	
	else
	{
		for(int i=0; i<49; i++)   //BUCLES ASIGNADORES PARA DISPONIBILIDAD
		{		
			if(pr[i].reg.cedula==0||pr[i].reg.cedula==1)
   			{
   				assig=i;
   				break;
   			}
   		}
		
		if(!(assig==50))
	    {
			pr[assig].con.codigo=51;
			pr[assig].con.diasrestantes=3285;
			pr[assig].reg.cedula=1602123341;
			pr[assig].reg.nombre="Ana";
			pr[assig].reg.apellido="Suarez";
			pr[assig].reg.edad=30;
    		pr[assig].con.cargos="Asesinato con cuchillo a su padre ";
    		printf("Recluso con ID %i y codigo %i agregado de manera predeterminada\n\n",assig,pr[assig].con.codigo);
    		system("pause");
    		system("cls");
    		acont++;
			count++;
			assig=50;
		}
		
		else
		{
			system("cls");
			printf("ERROR: No hay espacio para reclusos, la carcel esta llena, pero se ha agregado %i reclusos exitosamente\n\n",acont);
			system("pause");
			return;		
		}	
	}
	
	if(cedrev(1303753618))
	{
		system("cls");
	  	printf("ERROR: El cuarto preso predeterminado esta ya en el sistema\n");
	  	system("pause");
	}
	else
	{
		for(int i=0; i<49; i++)   //BUCLES ASIGNADORES PARA DISPONIBILIDAD
		{		
			if(pr[i].reg.cedula==0||pr[i].reg.cedula==1)
   			{
   				assig=i;
   				break;
   			}
   		}	
		
		if(!(assig==50))
   	 	{
			pr[assig].con.codigo=84;
			pr[assig].con.diasrestantes=1095;
			pr[assig].reg.cedula=1303753618;
			pr[assig].reg.nombre="Romeo";
			pr[assig].reg.apellido="Santos";
			pr[assig].reg.edad=60;
    		pr[assig].con.cargos="Allanamiento a una vivienda ";
    		printf("Recluso con ID %i y codigo %i agregado de manera predeterminada\n\n",assig,pr[assig].con.codigo);
    		system("pause");
    		system("cls");
    		acont++;
			count++;
			assig=50;
		}
		
		else
		{
			system("cls");
			printf("ERROR: No hay espacio para reclusos, la carcel esta llena, pero se ha agregado %i reclusos exitosamente\n\n",acont);
			system("pause");
			return;
	
		}
	}
	
	if(cedrev(1717385288))
	{
		system("cls");
	  	printf("ERROR: El quinto preso predeterminado esta ya en el sistema\n");
	  	system("pause");
	}
	else
	{
		for(int i=0; i<49; i++)   //BUCLES ASIGNADORES PARA DISPONIBILIDAD
		{		
			if(pr[i].reg.cedula==0||pr[i].reg.cedula==1)
   			{
   				assig=i;
   				break;
   			}
   		}	
		
		if(!(assig==50))
   		 {	
			pr[assig].con.codigo=10;
			pr[assig].con.diasrestantes=730;
			pr[assig].reg.cedula=1717385288;
			pr[assig].reg.nombre="Anahi";
			pr[assig].reg.apellido="Coloma";
			pr[assig].reg.edad=60;
    		pr[assig].con.cargos="Roba a mano armada";
    		printf("Recluso con ID %i y codigo %i agregado de manera predeterminada\n\n",assig,pr[assig].con.codigo);
    		system("pause");
    		system("cls");
    		acont++;
			count++;
			assig=50;
		}
		
		else
		{
			system("cls");
			printf("ERROR: No hay espacio para reclusos, la carcel esta llena, pero se ha agregado %i reclusos exitosamente\n\n",acont);
			system("pause");
			return;		
		}	
		
	}			
	
	if(cedrev(1303292583))
	{
		system("cls");
	  	printf("ERROR: El sexto preso predeterminado esta ya en el sistema\n");
	  	system("pause");
	}
	else
	{
		for(int i=0; i<49; i++)   //BUCLES ASIGNADORES PARA DISPONIBILIDAD
		{		
			if(pr[i].reg.cedula==0||pr[i].reg.cedula==1)
   			{
   				assig=i;
   				break;
   			}
   		}	
	
		if(!(assig==50))
   	 	{		
			pr[assig].con.codigo=88;
			pr[assig].con.diasrestantes=730;
			pr[assig].reg.cedula=1303292583;
			pr[assig].reg.nombre="Sebastian";
			pr[assig].reg.apellido="Sotelo";
			pr[assig].reg.edad=21;
    		pr[assig].con.cargos="Asesinato en un accidente de carro mato a 2 personas";
    		printf("Recluso con ID %i y codigo %i agregado de manera predeterminada\n\n",assig,pr[assig].con.codigo);
			count++;
			assig=50;
    		system("pause");
    		system("cls");
    
		}
		
		else
		{
			system("cls");
			printf("ERROR: No hay espacio para reclusos, la carcel esta llena, pero se ha agregado %i reclusos exitosamente\n\n",acont);
			system("pause");
			return;
			
		}
	}
	
	printf("Todos los prisioneros predeterminados han sido agregados exitosamente\n\n\n");
}
void nuevo_caso(Reclusos op[])
{
	int d,m,a,ver,control;
	int disp;
	int assig;
	int numeral;
	bool cedexs=false;
	bool val=false;
	int ID=0;
	int ag=0;
	system("cls");
	
	
	for(int i=0; i<49; i++)
	{		
		if((op[i].reg.cedula==0)||(op[i].reg.cedula==1)){}
   		else
   		{
   			disp++;
		}
   	}
   	
   	if(disp==49)
   	{
   		printf("ERROR: No hay espacio para reclusos, la carcel esta llena\n");
   		system("cls");
   		return;
 	}
	
	
	printf("\t\t===============REGISTRO CRIMINAL-NUEVO RECLUSO===============\n");
	
		
 	for(int i=0; i<49; i++)   //BUCLES ASIGNADORES PARA DISPONIBILIDAD
	{		
		if(op[i].reg.cedula==0||op[i].reg.cedula==1)
   		{
   			assig=i;
   			break;
   		}
	}

    printf("Se ha encontrado disponibilidad en el ID %i para el nuevo recluso\n",assig);
    ID=assig;
    assig=0;
    system("pause");
    system("cls");
	srand (time(0));
	numeral=rand() % 100 + 1 ;
	printf("El codigo del prisionero es CRJVG: %d\n",numeral);
	op[ID].con.codigo=numeral;
	
	system("pause");

	do
	{
		system("cls");
		printf("\nIngrese la cedula del nuevo recluso: ");
		cin>>ver;
		
		if(cedrev(ver))
		{
			cedexs=true;
			system("cls");
			printf("ERROR: Dicha cedula ya existe en el sistema\n");
			system("pause");
		}
		else
		{
				
			if(cedula(ver))
			{
				op[ID].reg.cedula=ver;
				system("cls");
				cedexs=false;
			}
			else
			{
				system("cls");
				printf("El numero de cedula esta errado o es falso\n");
				system("pause");
				system("cls");
				cedexs=true;

			}
		
		}
	
	}while(cedexs);
	
	
	printf("\nIngrese el nombre del nuevo recluso: ");
 	cin>>op[ID].reg.nombre;

	printf("\nIngrese el apellido del nuevo recluso: ");
 	cin>>op[ID].reg.apellido;

	do
	{
		printf("\nIngrese la edad: ");
		cin>>op[ID].reg.edad;
		control=op[ID].reg.edad;
		if((control<18)||(control>99))
		{
			printf("La edad no esta permitida o la edad no esta correcta");
			op[ID].reg.edad=0;
			control=0;
			system("pause");
			system("cls");
		}

	}while(control<18||(control>99));
	printf("\nIngrese el cargo del recluso: ");
	cin>>op[ID].con.cargos;
	fflush(stdin);
	time_t currentTime;
	time(&currentTime);
	system("cls");
	printf("\nTiempo de inicio de condena: %s\n",ctime(&currentTime));
	printf("\n========FECHA DE SALIDA========\n");
	printf("\nIngrese la fecha de salida\n");

	do
	{
		printf("\nAnio: 20");
		scanf("%d",&a);
		printf("\n");
			
		if(a<=17 || a>99)
		{
			val=true;
			printf("\nExcede del a�o permitido o dicho a�o es anterior a la fecha de hoy\n");
			a=0;
			system("pause");
			system("cls");
		}
		else if(a==18)
		{
			val=false;
			do
			{
				printf("Mes: ");
				scanf("%d",&m);
				fflush(stdin);

				if(m<=7 || m>12)
				{
					val=true;
					printf("\nEl mes ingresado no es valido o dicho mes es anterior del mes actual\n");
					m=0;
					system("pause");
					system("cls");
				}
				else if(m==8)
				{
					val=false;
					do
					{
						printf("\nDia: ");
						scanf("%d",&d);
						fflush(stdin);

						if(d<=13 || d>31)
						{
							val=true;
							printf("\nEl dia ingresado no es valido o dicho dia es anterior al dia actual\n");
							d=0;
							system("pause");
							system("cls");
						}
						else
						{
							val=false;
						}

					}while(val);
				}
				else
				{
						printf("\nDia: ");
						scanf("%d",&d);
						fflush(stdin);

						if(d<=01 || d>31)
						{
							val=true;
							printf("\nEl dia ingresado no es valido\n");
							d=0;
							system("pause");
							system("cls");
						}
						else
						{
							val=false;
						}
				}
				
			}while(val);
							
		}
		else
		{
			do
			{
				printf("\nMes: ");
				scanf("%d",&m);

				if(m<1 || m>12)
				{
					val=true;
					printf("\nEl mes ingresado no es valido\n");
					m=0;
					system("pause");
					system("cls");
				}
				else
				{
					val=false;
				}
			}while(val);
			
			do
			{
				printf("\nDia: ");
				scanf("%d",&d);

				if(d<1 || d>31)
				{
					val=true;
					printf("\nEl dia ingresado no es valido o dicho dia es anterior al dia actual\n");
					d=0;
					system("pause");
					system("cls");
				}
				else
				{
					val=false;
				}

			}while(val);
			
		}
	}while(val);

	
	if(a>=18)
	{
		double dias;
		time_t now;
		struct tm nuevo_a;
		double segundos;
		time(&now);
		nuevo_a = *localtime(&now);
		nuevo_a.tm_hour = 0; nuevo_a.tm_min = 0; nuevo_a.tm_sec = 0;
		nuevo_a.tm_year = (a+100);	nuevo_a.tm_mon = (m-1);  nuevo_a.tm_mday = (d+1);
		segundos = difftime(now,mktime(&nuevo_a));
		op[ID].con.diasrestantes= (0-segundos)/86400;
		
		printf ("\n\n\nQuedan %.f dias restantes de condena a partir de hoy.\n", 	op[ID].con.diasrestantes);	
	}
		system("pause");
		system("cls");
		count++;
		printf("Recluso agregado exitosamente, actualice el archivo de texto para ver los cambios\n\n");
		
}
void separar_cedula(int v[],int tam,long long int cedula)
{
    for(int i=(tam-1);i>=0;i--)
    {
        v[i]=cedula%10;
        cedula=cedula/10;
    }
}
void mostrar_cedula(int v[],int tam)
{
    for(int i=0;i<=(tam-1);i++)
    {
        cout<<v[i]<<endl;
    }
}
void inicializar_cedula(int v[],int tam)
{
    int a;
    for(a=0;a<tam;a++)
    {
        v[a]=0;
    }
}
bool validar_cedula(int v[],int tam,int ultimo)
{
    int r1=0,suma=0,r2=0,suma3=0,res=0,digval=0,resul=0;
    for(int i=0;i<=8;i+=2)
    {
        r1=v[i]*2;
        if(r1>9)
        {
            r1=r1-9;
        }
        suma=r1+suma;

    }
    for(int i=1;i<=7;i+=2)
    {
        r2=r2+v[i];
    }
    suma=r2+suma;
    resul=suma;
    res=((suma/10)+1)*10;
    digval=res-resul;
    if (digval==10)
    {
        digval=0;
    }
    if (digval==ultimo)
    {
        return true;

    }
    else
    {
        return false;
    }

}
bool cedula(long int cedula)
{
    int ced[TAM],ul,condi,cedu;
    bool valida;

     do
     {
            cedu=cedula;
            inicializar_cedula(ced,TAM);
        	separar_cedula(ced,TAM,cedula);
        }
        while((((ced[0]*10)+(ced[1]))<1 || ((ced[0]*10)+(ced[1]))>24) && (ced[TAM] > 99999999));

        cout<<cedu<<endl;
        ul=ced[9];
        valida=validar_cedula(ced,TAM,ul);

        if (valida)
        {
        	return true;

        }
        else
        {
        	return false;

        }
}
int main()
{
	
	init();
	menu1();
	return 0;
}
"# Homework2" 
